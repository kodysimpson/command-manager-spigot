package me.kodysimpson.prankplugin;

import me.kodysimpson.prankplugin.commands.CommandManager;
import org.bukkit.plugin.java.JavaPlugin;

public final class PrankPlugin extends JavaPlugin {

    @Override
    public void onEnable() {
        // Plugin startup logic

        getCommand("prank").setExecutor(new CommandManager());

    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }
}
